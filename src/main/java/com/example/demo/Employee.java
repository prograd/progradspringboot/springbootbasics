package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Employee {
private int eid;
    @Autowired//it calls no arg constructor
    Person personObj;

    public Employee(int eid, Person personObj) {
        this.eid = eid;
        this.personObj = personObj;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "eid=" + eid +
                ", personObj=" + personObj +
                '}';
    }

    public Employee() {

    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public Person getPersonObj() {
        return personObj;
    }

    public void setPersonObj(Person personObj) {
        this.personObj = personObj;
    }
}
