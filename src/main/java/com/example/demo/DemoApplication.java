package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		//Bean creation using annotation
		/*ConfigurableApplicationContext context=SpringApplication.run(DemoApplication.class, args);
		Student studentref=context.getBean(Student.class);
		studentref.display();*/

		//Bean creation using xml file
		/*ApplicationContext context=new ClassPathXmlApplicationContext("info.xml");
		Info infoObj=context.getBean("infoClass",Info.class);
		infoObj.display();*/

		ConfigurableApplicationContext context=SpringApplication.run(DemoApplication.class, args);
	    Employee employeeObj=context.getBean(Employee.class);
		System.out.println(employeeObj);
	}

}
