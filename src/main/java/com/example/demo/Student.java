package com.example.demo;

import org.springframework.stereotype.Component;

@Component//all ready serializable is applied by this annotation
public class Student {
    private String sname;
    private int standard;
    private int dividion;

    public Student() {
    }

    public Student(String sname, int standard, int dividion) {//optional to create
        this.sname = sname;
        this.standard = standard;
        this.dividion = dividion;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public int getStandard() {
        return standard;
    }

    public void setStandard(int standard) {
        this.standard = standard;
    }

    public int getDividion() {
        return dividion;
    }

    public void setDividion(int dividion) {
        this.dividion = dividion;
    }

    public void display(){
        System.out.println("Hi bean");
    }
}
