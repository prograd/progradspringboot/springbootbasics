package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public class Person {
    private String name;
    private String city;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Person() {//this wiil be called when object is created
        this.name="amar";
        this.city="Dimapur";
    }

    public Person(String name, String city) {
        this.name = name;
        this.city = city;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
    public void display(){
        System.out.println(name+" "+city);
    }
}
